import {Redirect, Route} from 'react-router-dom';
import {
    IonApp,
    IonRouterOutlet,
    setupIonicReact
} from '@ionic/react';
import {IonReactRouter} from '@ionic/react-router';
import Questions from './Pages/Questions';
import Result from './Pages/Result';
import Home from './Pages/Home';

import './theme/variables.css';
import '@ionic/react/css/core.css';
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';


setupIonicReact();

const Application: React.FC = () => (
    <IonApp>
        <IonReactRouter>
            <IonRouterOutlet>
                <Route exact path="/home" component={Home}/>
                <Route  path="/questions" component={Questions}/>
                <Route path="/result/:total" component={Result}/>
                <Redirect exact from="/" to="/home"/>
            </IonRouterOutlet>
        </IonReactRouter>
    </IonApp>
);

export default Application;
