import {IonButton, IonContent, IonPage} from '@ionic/react';
import './Result.css';
import React from 'react';
import {RouteComponentProps} from 'react-router';
import questions from '../Data/Questions.json';
import GaugeChart from 'react-gauge-chart'

interface RouteComponetPath {
    total?: string
}


class Result extends React.Component<RouteComponentProps<RouteComponetPath>> {


    getResult(total: any) {
        let range = questions.result;
        for (let i = 0; i < range.length; i++) {
            if (total >= range[i].min && total <= range[i].max) {
                return range[i].result;
            }
        }
        return '';
    }

    getColors(results: any) {
        const colors = [];
        for (let i = 0; i < results.length; i++) {
            colors.push(results[i].color);
        }
        return colors;
    }

    getMAxValues(results: any) {
        const maxValues = [];
        for (let i = 0; i < results.length; i++) {

            maxValues.push(((results[i].max - results[i].min) / 100));
        }
        return maxValues;
    }

    render() {
        const total = this.props.match.params.total;
        const result = this.getResult(total);
        const results = questions.result;
        const colors = this.getColors(results);
        const maxValues = this.getMAxValues(results);
        return (
            <div className='result-page'>
                <IonPage>
                    <IonContent color="primary">
                        <div className='center'>
                            <GaugeChart id="result"
                                        nrOfLevels={result.length}
                                        arcsLength={maxValues}
                                        colors={colors}
                                        percent={(Number(total) / 100)}
                                        arcPadding={0.03}
                            />
                            <div className='result-message'>
                                {result}
                            </div>
                            <IonButton href={'/questions/'} color="secondary">Nuovo questionario</IonButton>
                        </div>
                    </IonContent>
                </IonPage>
            </div>

        )
    }
}

export default Result;
