import {
    IonButton,
    IonContent,
    IonAlert,
    IonPage,
    IonHeader,
    IonToolbar
} from '@ionic/react';
import './Home.css';
import React from 'react';
import {App} from "@capacitor/app";



class Home extends React.Component{

    state = {
        open: false
    }

    setShowBackAlert() {
        this.setState({open: !this.state.open})
    }

    render() {

        return (
            <IonPage>
                <IonHeader>
                    <IonToolbar className={"main-header"} color="primary">
                        <h2>Nasal polyp calculator</h2>
                    </IonToolbar>
                </IonHeader>
                <IonContent  color="primary" >
                    <div className={"cover"}>
                        <div className={"button-container"}>
                            <IonButton href={'/questions/'} color="secondary" size="large">Inizia questionario</IonButton>
                        </div>
                    </div>
                    <IonAlert
                        isOpen={this.state.open}
                        header={'Conferma!'}
                        message={'Sei sicuro di voler chiudere l\'applicazione?'}
                        buttons={[
                            {
                                text: 'No',
                                role: 'cancel',
                                cssClass: 'secondary',
                                handler: () => {
                                }
                            },
                            {
                                text: 'si',
                                handler: () => {
                                    App.exitApp();
                                }
                            }
                        ]}
                        onDidDismiss={() => this.setShowBackAlert()}
                        cssClass='alert'
                    />
                </IonContent>
            </IonPage>

        )
    }
}

export default Home;
