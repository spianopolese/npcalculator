import React from 'react';
import {
    IonButton,
    IonIcon,
    IonContent,
    IonHeader,
    IonRange,
    IonPage,
    IonToolbar,
    IonLabel,
    IonToggle,
    IonAlert

} from '@ionic/react';
import './Questions.css';
import questions from '../Data/Questions.json';
import Progress from '../Components/Progress';
import {App} from '@capacitor/app';

//icons
import {chevronForwardOutline} from 'ionicons/icons';
import {chevronBackOutline} from 'ionicons/icons';

class Questions extends React.Component {


    state = {
        counter: 0,
        maxQuestions: questions.data.length,
        elements: [] as any,
        open: false
    }

    componentDidMount() {
        var datas = new Array(this.state.maxQuestions);

        for (let i = 0; i < datas.length; i++) {
            datas[i] = [];
            for (let j = 0; j < questions.data[i].numberQuestions; j++) {
                datas[i].push(0);
            }
        }
        this.setState({
            elements: datas
        })

        App.addListener('backButton', (event) => {
            this.setShowBackAlert();
        });
    }


    nextQuestion() {

        if (this.state.counter >= 0 && this.state.counter < this.state.maxQuestions - 1) {
            this.setState({
                counter: this.state.counter + 1
            })
        }
    }

    prevQuestion() {
        this.setState({
            counter: this.state.counter - 1
        })
    }

    setRangeValue(e: any, i: number, counter: number, max: number = 0) {
        let value = 0;

        if (e.detail.hasOwnProperty('checked')) {
            if (e.detail.checked === true) {
                value = max
            }
        } else {
            value = e.detail.value
        }
        if (this.state.elements.length) {
            const localElements = this.state.elements.slice();
            localElements[counter][i] = value;
            this.setState({
                elements: localElements
            })
        }
    }

    getResult() {
        const answers = this.state.elements;
        let total = 0;
        for (var i = 0; i < answers.length; i++) {
            for (var j = 0; j < answers[i].length; j++) {
                total += answers[i][j];
            }
        }
        return total;
    }

    setShowBackAlert() {
        this.setState({open: !this.state.open})
    }

    render() {
        const backButton = (this.state.counter > 0) ?
            <IonButton color="secondary" onClick={() => (this.prevQuestion())}><IonIcon
                icon={chevronBackOutline}/></IonButton> : '';
        const nextButton = (this.state.counter < this.state.maxQuestions - 1) ?
            <IonButton color="secondary" onClick={() => (this.nextQuestion())}> <IonIcon icon={chevronForwardOutline}/></IonButton> :
            <IonButton color="secondary" href={'/result/' + this.getResult()}>Risultato</IonButton>;

        return (

            <IonPage>
                <IonHeader>
                    <IonToolbar color="primary">
                        <Progress counter={this.state.counter + 1}></Progress>
                    </IonToolbar>
                </IonHeader>
                <IonContent className="ion-padding">
                    <h2 className='main-title'> {questions.data[this.state.counter].title}</h2>
                    <div className='questions'>
                        {questions.data[this.state.counter].questions.map((val, i) => {
                            if (val.type === "boolean") {
                                let isChecked = false;
                                if (this.state.elements.length) {
                                    isChecked = (this.state.elements[this.state.counter][i] > 0) ? true : false;
                                }
                                return (
                                    <div key={this.state.counter + '-' + i} className={"question"}>
                                        <IonLabel>{val.title}</IonLabel>
                                        <IonToggle
                                            onIonChange={(e) => this.setRangeValue(e, i, this.state.counter, val.max)}
                                            checked={isChecked}/>
                                    </div>)
                            } else {
                                let value = 0;
                                if (this.state.elements.length) {
                                    value = (this.state.elements[this.state.counter][i] > 0) ? this.state.elements[this.state.counter][i] : 0;
                                }
                                return (
                                    <div key={this.state.counter + '-' + i} className={"question"}>
                                        <IonLabel>{val.title}</IonLabel>
                                        <IonRange
                                            min={0}
                                            max={val.max}
                                            step={1}
                                            snaps={true}
                                            pin={true}
                                            value={value}
                                            onIonChange={(e) => this.setRangeValue(e, i, this.state.counter)}
                                        />
                                    </div>)
                            }
                        })}
                    </div>
                    <div className='actions'>
                        {backButton}
                        {nextButton}
                    </div>
                    <IonAlert
                        isOpen={this.state.open}
                        header={'Conferma!'}
                        message={'Sei sicuro di voler chiudere l\'applicazione?'}
                        buttons={[
                            {
                                text: 'No',
                                role: 'cancel',
                                cssClass: 'secondary',
                                handler: () => {
                                }
                            },
                            {
                                text: 'si',
                                handler: () => {
                                    App.exitApp();
                                }
                            }
                        ]}
                        onDidDismiss={() => this.setShowBackAlert()}
                        cssClass='alert'
                    />
                </IonContent>
            </IonPage>
        )
    }
}

export default Questions;
