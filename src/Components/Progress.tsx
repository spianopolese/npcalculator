import React from 'react';
import './Progress.css';
import questions from '../Data/Questions.json';

interface ProgressInterface {
    counter: number;
}

class Progress extends React.Component<ProgressInterface> {


    state = {
        currentQuestion: 0,
        maxQuestions: questions.data.length,
    }

    render() {
        let counter = this.props.counter;
        return (

            <>
                <ul className='progress-bar'>
                    {[...Array(this.state.maxQuestions)].map((x, i) =>
                        <li key={i} className={"progress-item" + (i < counter ? ' completed' : '')}
                            style={{width: `calc(${(100 / this.state.maxQuestions)}% - 5px)`}}/>
                    )}
                </ul>
            </>
        )
    }
}

export default Progress;
