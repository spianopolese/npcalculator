
# Nasal Polyps Calculator

Calculate Nasal Polyps risk after answering the questionnaire



## Authors

- [Macaronibros](https://www.macaronibros.com/)


## Requirements
Node v14.8.0

## Installation

Install dependencies

```bash
npm install
```

Install Ionic globally
```bash
sudo npm install -g @ionic/cli
```

Start server

```bash
cd npcalculator
sudo ionic serve -l
```

## Requirements to build App
- [Capacitor](https://capacitorjs.com/)

See [Build App with Capacitor](https://capacitorjs.com/docs/v2/basics/building-your-app)

## Android
Install [Android Studio](https://developer.android.com/studio)

Add Android
```bash
npx cap add android
```
Build App
```bash
npx cap copy
npx cap copy android
npx cap open android
```

Test on Device
- enable usb debug mode
```bash
ionic capacitor run android -l --external
```

## IOS
Requirements
- Xcode
- Cocoapods
```bash
sudo gem install cocoapods
```
Add IOS
```bash
npx cap add ios
```
Build App
```bash
npx cap copy
npm run build
npx cap copy
npx cap copy ios
npx cap open ios
```

See [Capacitor IOS](https://capacitorjs.com/docs/ios)

## Add icon and splash
See [Capacitor add icon and splash](https://capacitorjs.com/docs/guides/splash-screens-and-icons)

Android 
See [https://enappd.com/blog/icon-splash-in-ionic-react-capacitor-apps/114/](https://enappd.com/blog/icon-splash-in-ionic-react-capacitor-apps/114/)

In Unix Like systems
```bash
sudo chown -R USERNAME /usr/local/lib/node_modules/
```
Install Cordova Res globally

```bash
sudo npm install -g cordova-res
```
```bash
cordova-res ios --skip-config --copy
cordova-res android --skip-config --copy
```


## Customization
To change color palette generate with  [Ionic color generator](https://ionicframework.com/docs/theming/color-generator) 
and paste result into variables.css

## Tech Stack
**App:** React, Ionic 5, Cordova


