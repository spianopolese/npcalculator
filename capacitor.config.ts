import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'Nasal polyps calculator',
  webDir: 'build',
  bundledWebRuntime: false
};

export default config;
